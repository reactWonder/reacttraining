# ReactTraining

This repository is to maintain the assignment submissions during React Training

Branch name should be like : {employeeFirstName}_JsxAssignment1

Repository : https://gitlab.com/reactWonder/reacttraining.git
Assignment Content: https://drive.google.com/open?id=1fSActues31XUCBdnBfGdvMOiiFaFTzfw
Learning material : https://drive.google.com/open?id=1s2QP1q08e9ReiK28zkEx5T67uFbRvNe-


### JSX Assignment
1) Get the desgin to develop from Assignment Content link given above
2) Look for the image in shared directory for complete final design
3) You can get the images extracted for the help in assginment from the same shared directory
4) We only have to use the skill related to JSX with the use of function to return the JSX content
5) Think on the posiblity we can break down the desgin into smaller part that can be reuseable to complete the design

------------------------------------------------------------------------------------------------------------------------------